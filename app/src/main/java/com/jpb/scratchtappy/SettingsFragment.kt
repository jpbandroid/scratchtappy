package com.jpb.scratchtappy

import android.content.Intent
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        val signaturePreference: Preference? = findPreference("settings")
        signaturePreference?.setOnPreferenceClickListener {
            startActivity(Intent(requireContext(), SettingsActivity::class.java))
            true
        }
    }
}