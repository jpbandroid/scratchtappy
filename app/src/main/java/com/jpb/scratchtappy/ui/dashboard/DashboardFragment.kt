package com.jpb.scratchtappy.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jpb.scratchtappy.databinding.FragmentDashboardBinding
import com.jpb.scratchtappy.databinding.FragmentHomeBinding
import com.jpb.scratchtappy.ui.home.HomeViewModel

class DashboardFragment : Fragment() {
    var tap = 0

    private lateinit var dashboardViewModel: DashboardViewModel
    private var _binding: FragmentDashboardBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)

        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textDashboard
        dashboardViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        val root2 = inflater.inflate(com.jpb.scratchtappy.R.layout.fragment_dashboard, null) as ViewGroup
        val but = root.findViewById<View>(com.jpb.scratchtappy.R.id.button) as FloatingActionButton
        val text = root.findViewById<View>(com.jpb.scratchtappy.R.id.text3) as TextView
        but.setOnClickListener {
            tap++
            text.setText(tap.toString())
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}